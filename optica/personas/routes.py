from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from optica import db, bcrypt
from optica.models import Persona, Rol, Genero, Cuenta, Cliente, Colaborador, Paciente, Prevision
from optica.personas.forms import (RegistrationForm, LoginForm,UpdateAccountForm, RequestResetForm, ResetPasswordForm)
from optica.personas.utils import send_reset_email

personas = Blueprint('personas', __name__)



@personas.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RegistrationForm()

    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        cuenta = Cuenta(cuenta_email=form.email.data,cuenta_password=hashed_password)
        db.session.add(cuenta)
        db.session.commit()
        last_cuenta = Cuenta.query.order_by(Cuenta.cuenta_id.desc()).first()
        persona = Persona(id=form.rut.data, persona_nombre=form.nombre.data, persona_ap_paterno=form.apellido_p.data, persona_ap_materno=form.apellido_m.data, persona_telefono=form.telefono.data, genero_id = form.genero.data, cuenta_id=last_cuenta.cuenta_id)
        cliente = Cliente(id=form.rut.data)
        db.session.add(cliente)
        db.session.add(persona)
        db.session.commit()
        flash(f'Su cuenta ha sido creada satisfactoriamente', 'success')
        return redirect(url_for('personas.login'))
    return render_template('register.html', form=form)



@personas.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = LoginForm()
    if form.validate_on_submit():
        cuenta = Cuenta.query.filter_by(cuenta_email=form.email.data).first()
        if cuenta:
            user = Persona.query.filter_by(cuenta_id=cuenta.cuenta_id).first()
            if user and bcrypt.check_password_hash(cuenta.cuenta_password, form.password.data):
                login_user(user, remember=form.remember.data)
                next_page = request.args.get('next')
                flash(f'Sesión Iniciada correctamente', 'success')
                return redirect(next_page) if next_page else redirect(url_for('main.index'))
            else:
                flash('Inicio de secion fallido. Email y/o contraseña invalida', 'danger')
        else:
            flash('Inicio de secion fallido. Email y/o contraseña invalida', 'danger')
    return render_template('login.html', title='Login', form=form)




@personas.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('main.index'))





@personas.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    print("ACCOUNT")

    form = UpdateAccountForm()
    if form.validate_on_submit():
        current_user.persona_nombre = form.nombre.data
        current_user.persona_ap_paterno = form.apellido_p.data
        current_user.persona_ap_materno = form.apellido_m.data
        current_user.id = form.rut.data
        current_user.persona_telefono = form.telefono.data
        current_user.genero_id = form.genero.data
        current_user.email = form.email.data
        db.session.commit()
        flash('¡Tu cuenta ha sido actualizada!', 'success')
        return redirect(url_for('personas.account'))
    elif request.method == 'GET':
        form.nombre.data = current_user.persona_nombre
        form.apellido_p.data = current_user.persona_ap_paterno
        form.apellido_m.data = current_user.persona_ap_materno
        form.rut.data = current_user.id
        form.telefono.data = current_user.persona_telefono
        form.genero.data = current_user.genero_id
        persona=Persona.query.filter_by(id=current_user.id).first()
        cuenta=Cuenta.query.filter_by(cuenta_id=persona.cuenta_id).first()
        form.email.data = cuenta.cuenta_email
    return render_template('account.html', form=form)




@personas.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RequestResetForm()
    if form.validate_on_submit():
        cuenta = Cuenta.query.filter_by(cuenta_email=form.email.data).first()
        user = Persona.query.filter_by(cuenta_id=cuenta.cuenta_id).first()
        send_reset_email(user)
        flash('Se ha enviado un correo electrónico con instrucciones para restablecer su contraseña.', 'info')
        return redirect(url_for('personas.login'))
    return render_template('reset_request.html', title='Restablecer Contraseña', form=form)




@personas.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    user = Persona.verify_reset_token(token)
    if user is None:
        flash('Token inválido o vencido', 'warning')
        return redirect(url_for('personas.reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('¡Tu contraseña ha sido actualizada! Ahora puede iniciar sesión', 'success')
        return redirect(url_for('personas.login'))
    return render_template('reset_token.html', title='Restablecer Contraseña', form=form)
