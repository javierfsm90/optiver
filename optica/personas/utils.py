import os
from flask import url_for
from flask_mail import Message
from optica import mail
from optica.models import Persona, Rol, Genero, Cuenta, Cliente


def send_reset_email(user):
    cuenta = Cuenta.query.filter_by(cuenta_id=user.cuenta_id).first()

    token = user.get_reset_token()
    msg = Message('Solicitud de restablecimiento de contraseña',
                  sender='noreply@optiver.com',
                  recipients=[cuenta.cuenta_email])
    msg.body = f'''Para restablecer su contraseña, visite el siguiente enlace:
{url_for('personas.reset_token', token=token, _external=True)}
Si no realizó esta solicitud, simplemente ignore este correo electrónico y no se realizarán cambios.
'''
    mail.send(msg)
