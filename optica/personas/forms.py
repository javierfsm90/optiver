from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField, IntegerField
from wtforms.validators import DataRequired, Length, Email, EqualTo, Optional, ValidationError
from flask_login import current_user
from optica.models import Persona, Rol, Genero, Cuenta
from sqlalchemy import desc

class RegistrationForm(FlaskForm):
    nombre = StringField('Nombre',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    apellido_p = StringField('Apellido Paterno',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    apellido_m = StringField('Apellido Materno',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    rut = StringField('Rut',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    telefono = IntegerField('Telefono',
                            validators=[Optional()])
    genero = SelectField('Genero',
                               validators=[DataRequired("Este campo es requerido")],
                               choices=[
                                   ('', 'Genero'),
                                   (1, 'Masculino'),
                                   (2, 'Femenino'),
                               ],)

    email = StringField('Email',
                        validators=[DataRequired("Este campo es requerido"), Email("Direccion de correo invalido")])
    password = PasswordField('Password', validators=[DataRequired("Este campo es requerido")])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired("Este campo es requerido"), EqualTo('password', message='Las contraseñas deben coincidir')])

    submit = SubmitField('Registrarme')

    def validate_rut(self, rut):
        run = Persona.query.filter_by(id=rut.data).first()
        if run:
            raise ValidationError('El Rut ingresado ya se encuentra registrado')

    def validate_email(self, email):
        correo = Cuenta.query.filter_by(cuenta_email=email.data).first()
        if correo:
            raise ValidationError('El correo ingresado ya se encuentra en uso')


class LoginForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired("Este campo es requerido"), Email("Direccion de correo invalido")])
    password = PasswordField('Password', validators=[DataRequired("Este campo es requerido")])
    remember = BooleanField('Recuerdame')
    submit = SubmitField('Login')


class UpdateAccountForm(FlaskForm):
    nombre = StringField('Nombre',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    apellido_p = StringField('Apellido Paterno',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    apellido_m = StringField('Apellido Materno',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    rut = StringField('Rut',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    telefono = IntegerField('Telefono',
                            validators=[Optional()])
    genero = SelectField('Genero',
                               validators=[DataRequired("Este campo es requerido")],
                               choices=[
                                   ('', 'Genero'),
                                   (1, 'Masculino'),
                                   (2, 'Femenino'),
                               ],)

    email = StringField('Email',
                        validators=[DataRequired("Este campo es requerido"), Email("Direccion de correo invalido")])
    submit = SubmitField('Update')

    def validate_rut(self, rut):
        if current_user.id != rut.data:
            run = Persona.query.filter_by(id=rut.data).first()
            if run:
                raise ValidationError('El Rut ingresado ya se encuentra registrado')

    def validate_email(self, email):
        persona = Persona.query.get(current_user.id)
        cuenta = Cuenta.query.filter_by(cuenta_id=persona.cuenta_id).first()
        if cuenta.cuenta_email != email.data:
            correo = Cuenta.query.filter_by(cuenta_email=email.data).first()
            if correo:
                raise ValidationError('El correo ingresado ya se encuentra en uso')


class RequestResetForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired("Este campo es requerido"), Email("Direccion de correo invalido")])
    submit = SubmitField('Recuperar contraseña')

    def validate_email(self, email):
        user = Cuenta.query.filter_by(cuenta_email=email.data).first()
        if user is None:
            raise ValidationError('No hay una cuenta con ese correo electrónico. Primero debes registrarte.')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')
