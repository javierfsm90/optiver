from datetime import date, datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from optica import db, login_manager, app
from flask_login import UserMixin

@login_manager.user_loader
def load_user(user_id):
    return Persona.query.get(user_id)

class Cuenta(db.Model):
    cuenta_id  = db.Column(db.Integer, primary_key=True)
    cuenta_email = db.Column(db.String(120), unique=True, nullable=False)
    cuenta_password = db.Column(db.String(60), nullable=False)
    persona = db.relationship('Persona', lazy=True)

    def __repr__(self):
        return f"Cuenta('{self.cuenta_id}','{self.cuenta_email}','{self.cuenta_password}')"



class Persona(db.Model, UserMixin):
    id = db.Column(db.String(10), primary_key=True)
    persona_nombre = db.Column(db.String(20), unique=False, nullable=False)
    persona_ap_paterno = db.Column(db.String(20), unique=False, nullable=False)
    persona_ap_materno = db.Column(db.String(20), unique=False, nullable=False)
    persona_telefono = db.Column(db.Integer, unique=False, nullable=True)
    # image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    genero_id = db.Column(db.Integer, db.ForeignKey('genero.genero_id'), nullable=False)
    cuenta_id = db.Column(db.Integer,db.ForeignKey('cuenta.cuenta_id'), nullable=False)
    colaboradores = db.relationship('Colaborador', backref='workers', lazy=True)
    clientes = db.relationship('Cliente', backref='clients', lazy=True)
    pacientes = db.relationship('Paciente', backref='patients', lazy=True)


    def get_reset_token(self, expires_sec=1800):
        s = Serializer(app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return Persona.query.get(user_id)

    def __repr__(self):
        return f"Persona('{self.id}','{self.persona_nombre}','{self.persona_ap_paterno}','{self.persona_ap_materno}', '{self.genero_id}', '{self.persona_telefono}')"




class Genero(db.Model):
    genero_id = db.Column(db.Integer, primary_key=True)
    genero_nombre = db.Column(db.String(20), unique=True, nullable=False)
    personas = db.relationship('Persona', backref='people', lazy=True)

    def __repr__(self):
        return f"Genero('{self.genero_nombre}')"


class Colaborador(db.Model):
    colaborador_id = db.Column(db.Integer, primary_key=True)
    colaborador_fecha_nac = db.Column(db.Date(), nullable=False)
    colaborador_direccion = db.Column(db.String(50), nullable=False)
    id = db.Column(db.String(10), db.ForeignKey('persona.id'), nullable=False)
    rol_id = db.Column(db.Integer, db.ForeignKey('rol.rol_id'), nullable=False)
    prevision_id = db.Column(db.Integer, db.ForeignKey('prevision.prevision_id'), nullable=False)



    def __repr__(self):
        return f"Colaborador('{self.colaborador_fecha_nac}','{self.colaborador_direccion}'','{self.id}')"

class Cliente(db.Model):
    cliente_id = db.Column(db.Integer, primary_key=True)
    cliente_direccion = db.Column(db.String(50), nullable=True)
    id = db.Column(db.String(10), db.ForeignKey('persona.id'), nullable=False)

    def __repr__(self):
        return f"Cliente('{self.cliente_direccion}','{self.id}')"

class Paciente(db.Model):
    paciente_id = db.Column(db.Integer, primary_key=True)
    paciente_fecha_nac = db.Column(db.Date(), nullable=False)
    id = db.Column(db.String(10), db.ForeignKey('persona.id'), nullable=False)
    prevision_id = db.Column(db.Integer, db.ForeignKey('prevision.prevision_id'), nullable=False)

    def __repr__(self):
        return f"Paciente('{self.paciente_fecha_nac}','{self.id}')"


class Rol(db.Model):
    rol_id = db.Column(db.Integer, primary_key=True)
    rol_nombre = db.Column(db.String(20), unique=True, nullable=False)
    trabajadores = db.relationship('Colaborador', backref='roles', lazy=True)

    def __repr__(self):
        return f"Rol('{self.rol_nombre})"


class Prevision(db.Model):
    prevision_id = db.Column(db.Integer, primary_key=True)
    prevision_nombre = db.Column(db.String(20), unique=True, nullable=False)
    previsionados = db.relationship('Colaborador', backref='provisioned', lazy=True)

    def __repr__(self):
        return f"Prevision('{self.prevision_nombre}')"
