from flaskext.mysql import MySQL
import pymysql
import pymysql.cursors
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
# from optica.config import Config
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY')
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_URI')
app.config['SQLALCHEMY_DATABASE_URI'] ='mysql+pymysql://root@localhost/optiver'
SQLALCHEMY_TRACK_MODIFICATIONS = False
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'personas.login'
login_manager.login_message_category = 'info'
login_manager.login_message = 'Inicie sesión para acceder a esta página.'
app.config['MAIL_SERVER'] = 'smtp.googlemail.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = True
# app.config['MAIL_USERNAME'] = os.environ.get('EMAIL_USER')
# app.config['MAIL_PASSWORD'] = os.environ.get('EMAIL_PASS')
app.config['MAIL_USERNAME'] = 'javierfsm23@gmail.com'
app.config['MAIL_PASSWORD'] = 'alarico2312'
mail = Mail(app)

from optica.personas.routes import personas
from optica.main.routes import main
from optica.admin.routes import admin
from optica.errors.handlers import errors
app.register_blueprint(personas)
app.register_blueprint(main)
app.register_blueprint(admin)
app.register_blueprint(errors)


# app = Flask(__name__)

# db = SQLAlchemy()
# bcrypt = Bcrypt()
# login_manager = LoginManager()
# login_manager.login_view = 'personas.login'
# login_manager.login_message_category = 'info'
# login_manager.login_message = 'Inicie sesión para acceder a esta página.'
# mail = Mail()

# def create_app(config_class=Config):
#     app = Flask(__name__)
#     app.config.from_object(Config)
#     db.init_app(app)
#     bcrypt.init_app(app)
#     login_manager.init_app(app)
#     mail.init_app(app)

#     from optica.personas.routes import personas
#     from optica.main.routes import main
#     app.register_blueprint(personas)
#     app.register_blueprint(main)

#     return app

# print("SQLALCHEMY_DATABASE_URI")
# print(os.environ.get('SQLALCHEMY_DATABASE_URI'))
# print("SECRET_KEY")
# print(os.environ.get('SECRET_KEY'))
