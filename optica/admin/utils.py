import os
from flask import url_for
from flask_mail import Message
from optica import mail

def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Solicitud de restablecimiento de contraseña',
                  sender='noreply@optiver.com',
                  recipients=[user.email])
    msg.body = f'''Para restablecer su contraseña, visite el siguiente enlace:
{url_for('personas.reset_token', token=token, _external=True)}
Si no realizó esta solicitud, simplemente ignore este correo electrónico y no se realizarán cambios.
'''
    mail.send(msg)
