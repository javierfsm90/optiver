from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField, IntegerField, SelectMultipleField
from wtforms.fields.html5 import DateField
from wtforms_sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired, Length, Email, EqualTo, Optional, ValidationError
from flask_login import current_user
from optica.models import Persona, Rol, Genero, Cuenta, Cliente, Colaborador, Paciente, Prevision
from sqlalchemy import desc


class LoginFormadmin(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired("Este campo es requerido"), Email("Direccion de correo invalido")])
    password = PasswordField('Password', validators=[DataRequired("Este campo es requerido")])
    remember = BooleanField('Recuerdame')
    submit = SubmitField('Login')


class AddPersonAccountForm(FlaskForm):
    nombre = StringField('Nombre',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    apellido_p = StringField('Apellido Paterno',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    apellido_m = StringField('Apellido Materno',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    rut = StringField('Rut',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    telefono = IntegerField('Telefono',
                            validators=[Optional()])
    genero = SelectField('Genero',
                               validators=[DataRequired("Este campo es requerido")],
                               choices=[
                                   ('', '< Seleccione Genero >'),
                                   (1, 'Masculino'),
                                   (2, 'Femenino'),
                               ],)

    email = StringField('Email',
                        validators=[DataRequired("Este campo es requerido"), Email("Direccion de correo invalido")])

    nacimiento = DateField('Fecha de nacimiento')

    rol = SelectField('Rol',
                               validators=[DataRequired("Este campo es requerido")],
                               choices=[
                                   ('', '< Seleccione Rol >'),
                                   (1, 'Gerente'),
                                   (2, 'Vendedor'),
                                   (3, 'Tecnologa Medica'),
                               ],)

    prevision = SelectField('Prevision',
                               validators=[DataRequired("Este campo es requerido")],
                               choices=[
                                   ('', '< Seleccione Prevision >'),
                                   (1, 'Fonasa'),
                                   (2, 'Banmédica'),
                                   (3, 'Chuquicamata Ltda'),
                                   (4, 'Colmena Golden Cross'),
                                   (5, 'Consalud'),
                                   (6, 'Cruz Blanca'),
                                   (7, 'Cruz del Norte'),
                                   (8, 'Nueva Masvida'),
                                   (9, 'Fundación '),
                                   (10, 'Fusat '),
                                   (11, 'Río Blanco'),
                                   (13, 'San Lorenzo '),
                                   (13, 'Vida Tres '),
                               ],)


    direccion = StringField('Direccion',
                           validators=[DataRequired("Este campo es requerido"), Length(min=10, max=60, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    submit = SubmitField('Registrarme')

    def validate_rut(self, rut):
        run = Persona.query.filter_by(id=rut.data).first()
        if run:
            raise ValidationError('El Rut ingresado ya se encuentra registrado')

    def validate_email(self, email):
        correo = Cuenta.query.filter_by(cuenta_email=email.data).first()
        if correo:
            raise ValidationError('El correo ingresado ya se encuentra en uso')




class UpdatePersonAccountForm(FlaskForm):
    nombre = StringField('Nombre',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    apellido_p = StringField('Apellido Paterno',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    apellido_m = StringField('Apellido Materno',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    rut = StringField('Rut',
                           validators=[DataRequired("Este campo es requerido"), Length(min=2, max=20, message="El campo debe tener entre %(min)s y %(max)s caracteres")])
    telefono = IntegerField('Telefono',
                            validators=[Optional()])
    genero = SelectField('Genero',
                               validators=[DataRequired("Este campo es requerido")],
                               choices=[
                                   ('', 'Genero'),
                                   (1, 'Masculino'),
                                   (2, 'Femenino'),
                               ],)
    rol = SelectField('Rol',
                               validators=[DataRequired("Este campo es requerido")],
                               choices=[
                                   ('', 'Rol'),
                                   (1, 'Gerente'),
                                   (2, 'Vendedor'),
                                   (3, 'Cliente'),
                               ],)


    email = StringField('Email',
                        validators=[DataRequired("Este campo es requerido"), Email("Direccion de correo invalido")])
    submit = SubmitField('Update')


    def validate_email(self, email):
        cuenta = Cuenta.query.filter_by(cuenta_email=email.data).first()
        if cuenta.cuenta_email != email.data:
            correo = Cuenta.query.filter_by(cuena_email=email.data).first()
            if correo:
                raise ValidationError('El correo ingresado ya se encuentra en uso')
