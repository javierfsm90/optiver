from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from optica import db, bcrypt
from optica.models import Persona, Rol, Genero, Cuenta, Cliente, Colaborador, Paciente, Prevision
from optica.admin.forms import (AddPersonAccountForm,UpdatePersonAccountForm,LoginFormadmin)
# from optica.admin.utils import send_reset_email


admin = Blueprint('admin', __name__)

@admin.route('/admin/')
@admin.route('/admin')
@admin.route('/admin/index')
@login_required
def index():
    return render_template("admin/index.html")

@admin.route("/admin/addPerson", methods=['GET', 'POST'])
@admin.route("/admin/addPerson/", methods=['GET', 'POST'])
@login_required
def addPerson():
    form = AddPersonAccountForm()
    if form.validate_on_submit():

        password = form.rut.data
        hashed_password = bcrypt.generate_password_hash(password).decode('utf-8')
        cuenta = Cuenta(cuenta_email=form.email.data,cuenta_password=hashed_password)
        db.session.add(cuenta)
        db.session.commit()
        last_cuenta = Cuenta.query.order_by(Cuenta.cuenta_id.desc()).first()
        persona = Persona(id=form.rut.data, persona_nombre=form.nombre.data, persona_ap_paterno=form.apellido_p.data, persona_ap_materno=form.apellido_m.data, persona_telefono=form.telefono.data, genero_id = form.genero.data, cuenta_id=last_cuenta.cuenta_id)
        db.session.add(persona)
        db.session.commit()
        colaborador = Colaborador(colaborador_fecha_nac=form.nacimiento.data, colaborador_direccion=form.direccion.data,id=form.rut.data,rol_id=form.rol.data,prevision_id=form.prevision.data)
        db.session.add(colaborador)
        db.session.commit()
        flash(f'Su cuenta ha sido creada satisfactoriamente', 'success')
        return redirect(url_for('admin.index'))
    return render_template('admin/addPerson.html', form=form)


@admin.route('/admin/editPerson/', methods=['GET', 'POST'])
@admin.route('/admin/editPerson', methods=['GET', 'POST'])
@login_required
def editPerson():
    form = UpdatePersonAccountForm()
    personas_ = db.session.query(Persona).join(Colaborador, Persona.id==Colaborador.id).all()
    return render_template("admin/editPerson.html" , form=form, personas=personas_)

@admin.route('/admin/editPerson/<id>/', methods=['GET', 'POST'])
@admin.route('/admin/editPerson/<id>', methods=['GET', 'POST'])
@login_required
def editPersonId(id):
    form = UpdatePersonAccountForm()
    personas_ = db.session.query(Persona).join(Colaborador, Persona.id==Colaborador.id).all()
    persona_ = Persona.query.get(id)
    my_data = Persona.query.get(id)

    if id=='0' or Colaborador.query.filter_by(id=id).first() is None:
        flash('¡Colaborador no encontrado!', 'info')
        return render_template("admin/editPerson.html" , form=form, personas=personas_, persona=persona_)

    else:
        cuenta=Cuenta.query.filter_by(cuenta_id=persona_.cuenta_id).first()
        if form.validate_on_submit():
            if id==form.rut.data:
                my_data.persona_nombre = form.nombre.data
                my_data.persona_ap_paterno = form.apellido_p.data
                my_data.persona_ap_materno = form.apellido_m.data
                my_data.id = form.rut.data
                my_data.persona_telefono = form.telefono.data
                my_data.genero_id = form.genero.data
                my_data.rol_id = form.rol.data
                my_data.rol_id = form.multipleselect.data
                my_data.email = form.email.data

                db.session.commit()
                flash('¡Cuenta ha sido actualizada correctamente!', 'success')

            elif Persona.query.filter_by(id=form.rut.data).first():
                flash('¡El rut ingresado ya existe!', 'warning')
            else:
                my_data.persona_nombre = form.nombre.data
                my_data.persona_ap_paterno = form.apellido_p.data
                my_data.persona_ap_materno = form.apellido_m.data
                my_data.id = form.rut.data
                my_data.persona_telefono = form.telefono.data
                my_data.genero_id = form.genero.data
                my_data.rol_id = form.rol.data
                my_data.rol_id = form.multipleselect.data
                my_data.email = form.email.data

                db.session.commit()
                flash('¡Cuenta ha sido actualizada correctamente!', 'success')

        elif request.method == 'GET':
            form.nombre.data = my_data.persona_nombre
            form.apellido_p.data = my_data.persona_ap_paterno
            form.apellido_m.data = my_data.persona_ap_materno
            form.rut.data = my_data.id
            form.telefono.data = my_data.persona_telefono
            form.genero.data = my_data.genero_id
            cuenta=Cuenta.query.filter_by(cuenta_id=persona_.cuenta_id).first()
            form.email.data = cuenta.cuenta_email

        return render_template("admin/editPerson.html" , form=form, personas=personas_, persona=persona_)


@admin.route("/admin/login", methods=['GET', 'POST'])
def login():
        if current_user.is_authenticated:
            return redirect(url_for('admin.index'))
        form = LoginFormadmin()
        if form.validate_on_submit():
            cuenta = Cuenta.query.filter_by(cuenta_email=form.email.data).first()
            if cuenta:
                user = Persona.query.filter_by(cuenta_id=cuenta.cuenta_id).first()
                if user and bcrypt.check_password_hash(cuenta.cuenta_password, form.password.data):
                    login_user(user, remember=form.remember.data)
                    next_page = request.args.get('next')
                    flash(f'Sesión Iniciada correctamente', 'success')
                    return redirect(next_page) if next_page else redirect(url_for('admin.index'))
                else:
                    flash('Inicio de secion fallido. Email y/o contraseña invalida', 'danger')
            else:
                flash('Inicio de secion fallido. Email y/o contraseña invalida', 'danger')
        return render_template('admin/login.html', title='Login', form=form)


@admin.route("/admin/logout")
def logout():
    logout_user()
    return redirect(url_for('admin.index'))
